import React from "react";


class Box extends React.Component{
    constructor(props) {
      super(props);
      this.state = {boxColor:'gray'}
      this.colorChange = this.colorChange.bind(this);
      }
    
  
    colorChange() {
      var rand = "#" + (Math.floor(Math.random() * 16777215).toString(16));
      this.setState({boxColor : rand})
      console.log(rand)
  
    }
    render() {
      
      
      return (
        
            <div className="Box1" onMouseEnter={this.colorChange} style={{backgroundColor:this.state.boxColor}}/>
          
      );
    }
  }

  export default Box;